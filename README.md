# Hello World example of Spring Boot / Eureka / Docker

## Prerequisites
- docker installed
- docker compose installed
- maven installed
- mvn packages build (you can use ./mvnb for this)

## Running the application

from the main directory (containing docker-compose.yml)
```
docker-compose up
```

## Testing the application

Web-UI for Eureka:

http://localhost:8761

Calling the hello-client application

http://localhost:8092/hello/some_value


After starting the containers it will take some time until the registration at eureka happens. The hello-call will start to work at this moment.