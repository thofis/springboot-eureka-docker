package thofis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
public class HelloClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloClientApplication.class, args);
    }
}

@SuppressWarnings("SpringJavaAutowiringInspection")
@RestController
class HelloClientController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("hello/{name}")
    public String getHello(@PathVariable("name") final String name) {
        return getGreeting() + " " + name;
    }

    private String getGreeting() {
        return restTemplate.getForObject("http://hello-service/greeting", String.class);
    }

}
